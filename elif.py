
# coding: utf­8
edad = int(input("Indique su edad: "))
sexo = input("Indique su sexo (H/M): ")
color_pelo = input("Indique su color de pelo (RUBIO/MORENO/PELIROJO): ")
if edad < 18 :
    print("Lo siento con " , edad , " no puedes pasar")
# Se cumple:   edad >= 18 and sexo == "M"
elif sexo == "M" :
    print("Bienvenida señora")
# Se cumple:   edad >= 18 and sexo <> "M" and color_pelo == "RUBIO"
elif color_pelo == "RUBIO" :
    print("Bienvenid señor")
    print("Bonito pelo rubio")
# Se cumple:   edad >= 18 and sexo <> "M" and color_pelo <> "RUBIO" and
#              color_pelo == "MORENO"
elif color_pelo == "MORENO" :
    print("Bienvenid señor")
    print("Bonito pelo moreno")
# Se cumple:   edad >= 18 and sexo <> "M" and color_pelo <> "RUBIO" and
#              color_pelo <> "MORENO" and color_pelo == "PELIROJO"
elif color_pelo == "PELIROJO" :
    print("Bienvenido señor")
    print("Bonito pelo pelirojo")
# Se cumple:   edad < 18 and sexo <> "M" and color_pelo <> "RUBIO" and
#              color_pelo <> "MORENO" and color_pelo <> "PELIROJO"
else:
    print("Bienvenido señor")
    print("Qué pelo es ese?")